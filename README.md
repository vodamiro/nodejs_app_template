# Node.js Console and Web App template project

Commands:

`npm start A B C D` - run console app with parameters `A B C D`

`npm run web PORT` - run web app on `PORT`

`docker build -t foo . && docker run -it foo` - build Docker image and run it