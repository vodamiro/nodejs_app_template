import doit from './doit/doit'

function run() {
    // Check argument count
    if (process.argv.length < 4) {
        console.log("Error: Wrong arguments")
        console.log("")
        console.log("Usage: node index.js A B C D")
        process.exit(1)
    }

    // Load parameters
    const a = process.argv[2]
    const b = process.argv[3]
    const c = process.argv[4] || "" // default value
    const d = Number(process.argv[5]) || 1 // default value

    // TODO
    doit()
}

run()